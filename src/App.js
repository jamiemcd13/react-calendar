import React from 'react';
import Calendar from './components/Calendar';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div className="section">
            <div className="container">
              <Route path="/:month?/:year?" component={Calendar} />
            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
