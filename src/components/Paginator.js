import React from 'react';
import PropTypes from 'prop-types';

const Paginator = ({ title, onPrevClick, onNextClick, isPrevEnabled, isNextEnabled }) => (
  <div className="paginator">
    <nav className="level">
      <div className="level-item">
        <div className="buttons has-addons">
          <button
            className={`button paginator__button paginator__prev ${
              !isPrevEnabled ? 'paginator__button--disabled' : ''
            }`}
            disabled={!isPrevEnabled}
            onClick={() => (isPrevEnabled ? onPrevClick() : false)}
          >
            <span className="icon">
              <i className="fas fa-chevron-left" />
            </span>
          </button>
          <button
            className={`button paginator__button paginator__next ${
              !isNextEnabled ? 'paginator__button--disabled' : ''
            }`}
            disabled={!isNextEnabled}
            onClick={() => (isNextEnabled ? onNextClick() : false)}
          >
            <span className="icon">
              <i className="fas fa-chevron-right" />
            </span>
          </button>
        </div>
      </div>
      <div className="level-item">{title}</div>
    </nav>
  </div>
);

Paginator.propTypes = {
  title: PropTypes.string,
  onPrevClick: PropTypes.func,
  onNextClick: PropTypes.func,
  isPrevEnabled: PropTypes.bool,
  isNextEnabled: PropTypes.bool
};

export default Paginator;
