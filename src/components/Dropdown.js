import React from 'react';
import PropTypes from 'prop-types';
import DropdownOption from './DropdownOption';

class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isActive: false };
    this.handleClick = this.handleClick.bind(this);
    this.handleOptionClick = this.handleOptionClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClick() {
    this.setState(prevState => ({ isActive: !prevState.isActive }));
  }

  handleOptionClick(label) {
    this.props.onChange(label);
  }

  handleChange(option) {
    this.props.onChange(option);
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      console.log('clicked outside');
      this.setState({ isActive: false });
    }
  }

  render() {
    return (
      <div
        className={`dropdown is-right ${this.state.isActive ? 'is-active' : ''}`}
        onClick={this.handleClick}
        ref={this.setWrapperRef}
      >
        <div className="dropdown-trigger">
          <button className="button" aria-haspopup="true" aria-controls="dropdown-menu">
            <span>{this.props.title}</span>
            <span className="icon is-small">
              <i className="fas fa-angle-down" aria-hidden="true" />
            </span>
          </button>
        </div>
        <div className="dropdown-menu" id="dropdown-menu" role="menu">
          <div className="dropdown-content">
            {this.props.options.map(option => (
              <DropdownOption
                key={option.label + option.selected.toString()}
                label={option.label}
                selected={option.selected}
                onClick={this.handleOptionClick}
              />
            ))}
          </div>
        </div>
      </div>
    );
  }
}

Dropdown.propTypes = {
  title: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      selected: PropTypes.bool
    })
  ),
  onChange: PropTypes.func
};

export default Dropdown;
