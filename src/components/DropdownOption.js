import React from 'react';
import PropTypes from 'prop-types';

class DropdownOption extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.label);
  }

  render() {
    return (
      <a onClick={this.handleClick} className={`dropdown-item ${this.props.selected ? 'is-active' : ''}`}>
        <span>{this.props.label}</span>
      </a>
    );
  }
}

DropdownOption.propTypes = {
  label: PropTypes.string,
  selected: PropTypes.bool
};

export default DropdownOption;
