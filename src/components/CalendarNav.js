import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Paginator from './Paginator';
import Dropdown from './Dropdown';

class CalendarNav extends React.Component {
  constructor(props) {
    super(props);
    this.decrementDate = this.decrementDate.bind(this);
    this.incrementDate = this.incrementDate.bind(this);
    this.handleMonthChange = this.handleMonthChange.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
  }

  decrementDate() {
    this.props.onDateChange(this.props.date.clone().subtract(1, 'months'));
  }

  incrementDate() {
    this.props.onDateChange(this.props.date.clone().add(1, 'months'));
  }

  handleMonthChange(month) {
    this.props.onDateChange(this.props.date.clone().month(month));
  }

  handleYearChange(year) {
    this.props.onDateChange(this.props.date.clone().year(year));
  }

  render() {
    return (
      <nav className="calendar__nav level">
        <div className="level-left">
          <div className="level-item">
            <Paginator
              title={this.props.date.format('MMMM YYYY')}
              onPrevClick={this.decrementDate}
              onNextClick={this.incrementDate}
              isPrevEnabled={this.props.date.isAfter(moment(this.props.years[0], 'YYYY'))}
              isNextEnabled={this.props.date.isBefore(
                moment(this.props.years[this.props.years.length - 1], 'YYYY')
                  .endOf('year')
                  .subtract(1, 'months')
              )}
            />
          </div>
        </div>
        <div className="level-right">
          <div className="level-item">
            <Dropdown
              title="Months"
              options={moment.months().map(month => ({
                label: month,
                selected: month === this.props.date.clone().format('MMMM')
              }))}
              onChange={this.handleMonthChange}
            />
          </div>
          <div className="level-item">
            <Dropdown
              title="Year"
              options={this.props.years.map(year => ({
                label: year,
                selected: year === this.props.date.clone().format('YYYY')
              }))}
              onChange={this.handleYearChange}
            />
          </div>
        </div>
      </nav>
    );
  }
}

CalendarNav.propTypes = {
  date: PropTypes.instanceOf(moment).isRequired,
  years: PropTypes.array,
  onDateChange: PropTypes.func
};

export default CalendarNav;
