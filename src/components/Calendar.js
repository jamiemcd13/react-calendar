import React from 'react';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import CalendarNav from './CalendarNav';
import CalendarGrid from './CalendarGrid';

const moment = extendMoment(Moment);

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment(),
      years: Array.from(
        moment()
          .range(
            moment()
              .startOf('year')
              .subtract(30, 'years'),
            moment()
              .startOf('year')
              .add(30, 'years')
          )
          .by('year')
      ).map(y => y.format('YYYY'))
    };
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  componentWillMount() {
    moment.locale('en', {
      week : {
        dow : 1 // Set Monday as the first day of the week
      }
    });
  }

  componentDidMount() {
    const date =
      this.props.match.params.month && this.props.match.params.year
        ? moment()
            .year(this.props.match.params.year)
            .month(this.props.match.params.month)
            .startOf('month')
        : this.state.date;

    this.handleDateChange(date, true); // Set the default params in the URL
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.match.params.month !== prevProps.match.params.month ||
      this.props.match.params.year !== prevProps.match.params.year
    ) {
      this.setState({
        date: moment()
          .year(this.props.match.params.year)
          .month(this.props.match.params.month)
          .startOf('month')
      });
    }
  }

  handleDateChange(date, replaceHistory = false) {
    const url = '/' + date.format('MMMM').toLowerCase() + '/' + date.format('YYYY');
    this.setState({ date });
    replaceHistory ? this.props.history.replace(url) : this.props.history.push(url);
  }

  render() {
    return (
      <div className="calendar">
        <CalendarNav date={this.state.date} years={this.state.years} onDateChange={this.handleDateChange} />
        <CalendarGrid date={this.state.date} />
      </div>
    );
  }
}

export default Calendar;
