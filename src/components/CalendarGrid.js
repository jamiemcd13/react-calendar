import React, { Component } from 'react';
import moment from 'moment';

class CalendarGrid extends Component {
  constructor(props) {
    super(props);
    this.state = { dates: [] };
    this.setDates = this.setDates.bind(this);
  }

  componentWillMount() {
    this.setDates();
  }

  componentDidUpdate(prevProps) {
    if (this.props.date !== prevProps.date) {
      this.setDates();
    }
  }

  setDates() {
    const chosenMonth = this.props.date.clone().startOf('month');
    const weekdayIndex = chosenMonth.clone().weekday();
    const firstDate = chosenMonth.clone().subtract(weekdayIndex, 'days');

    const dates = Array.from(Array(35), (v, i) => {
      let date = firstDate.clone().add(i, 'day');
      let isDisabled = !moment(date).isSame(chosenMonth, 'month');
      return { key: date.format('YYYY-MM-DD'), label: date.get('date'), isDisabled };
    });

    this.setState({ dates });
  }

  render() {
    return (
      <div className="calendar__grid tile is-ancestor">
        {moment.weekdays(true).map(day => (
          <div className="calendar__grid-tile calendar__grid-header tile" key={day}>
            {day}
          </div>
        ))}
        {this.state.dates.map(date => (
          <div
            className={`calendar__grid-tile tile ${date.isDisabled ? 'calendar__grid-tile--disabled' : ''}`}
            key={date.key}
          >
            <div className="calendar__grid-tile-content">{date.label}</div>
          </div>
        ))}
      </div>
    );
  }
}

export default CalendarGrid;
